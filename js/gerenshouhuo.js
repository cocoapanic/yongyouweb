$(document).ready(function() {
    //添加汽车上的卸货按钮
    $('.shouhuo-container').delegate('.shouhuo-box', 'mouseenter', function(ev) {
        $(this).children('.shouhuoanniu').css('display', 'block');
    });
    $('.shouhuo-container').delegate('.shouhuo-box', 'mouseleave', function() {
        $(this).children('.shouhuoanniu').css('display', 'none');
    });

    $('.shouhuo-container').delegate('.shouhuoanniu', 'click', function(ev) {

        var goodPopover = $('#good-detail');
        showGoodsPopover(goodPopover, ev, 'right');
    });


    /*=============================
        关闭弹窗
    =============================*/
    $('#close-detail').click(function() {
        hidePopovers();
    });

    /*=============================
        确认收货
    =============================*/
    $('.shouhuo-cancle').click(function() {
        // 在这里执行用户的收货操作，与后台数据交互

        
        hidePopovers(); // 隐藏货物详情框
    });

});

function showGoodsPopover(obj, event, type) {
    hidePopovers();
    obj.css('display', 'block');
    obj.css('top', event.pageY + 'px');
    switch (type) {
    case "left":
        obj.css('left', (event.pageX - obj.width()) + 'px');
        break;
    case "right":
        obj.css('left', event.pageX + 'px');
        break;
    default:
        obj.css('left', (event.pageX - obj.width() / 2) + 'px');
    }
}
function hidePopovers() {
    $('#good-detail').css('display', 'none');
}