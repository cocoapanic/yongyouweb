$(document).ready(function() {
    $("input:radio[name='yongtu']").change(function(event) {
        var $fks = $("#fk-sel");
        var $fki = $("#fk-input");

        if ($(this).val() === "1") {
            $fki.hide();
            $fks.show();
        }else {
            $fks.hide();
            $fki.show();
        }
    });
	/**************************
		设置模态框
	**************************/
	$('#skr-model-trigger').leanModal({
        top: 110,
        overlay: 0.45,
        closeButton: "#hidemodal"
    });

    /**************************
		为收款人名册绑定点击事件
	**************************/
	$('.skr-list-content ul').delegate('li', 'click',
    function(ev) {
        
        // text
        var option = $(this).find('p span');
       	$('#skrzh').val(option[1].innerHTML);
       	$('#skrhm').val(option[0].innerHTML);
        hideModel();
    });
});

function hideModel () {
	$("#hidemodal").trigger('click');
}