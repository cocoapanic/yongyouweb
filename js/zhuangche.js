var currentDrag;
var currentDrop;
var currentBox;
$(document).ready(function() {
	//添加鼠标移入和移出事件，通过delegate方法可以实现通过js动态添加的元素自动绑定事件
    $('.baozhuang-container').delegate('.good', 'mouseenter',
    function(ev) {
        var goodPopover = $('#good-detail');
        showGoodsPopover(goodPopover, ev, 'right');

    });
    $('.baozhuang-container').delegate('.good', 'mouseleave',
    function(ev) {
        var goodId = $(this).attr('data-id');
        $('#good-detail').css('display', 'none');
    });


    $('.baozhuang-container').delegate('.popover-right-box', 'mouseenter',
    function(ev) {
    	currentBox = $(this);
        var packetID = $(this).attr('data-id');
        var goodPopover = $('#box-detail');
        showGoodsPopover(goodPopover, ev, 'right');
    });
	$('.baozhuang-container').delegate('.popover-right-box2', 'mouseenter',
    function(ev) {
        var packetID = $(this).attr('data-id');
        var goodPopover = $('#box-detail-nomodify');
        showGoodsPopover(goodPopover, ev, 'right');
    });

    /*=============================
        关闭弹窗
    =============================*/
    $('#baozhuang-bg').click(function() {
        hidePopovers();
    });

    $('#cancle-alert').click(function() {
        currentDrag.css({
            "top": 0,
            "left": 0
        });
        currentDrag.draggable("option", "revert", true);
        var modifynumber = $('#alert');
        modifynumber.css('display', 'none');
    });

    /*=============================
        处理放下事件
    =============================*/
	$('.goods-bg-big').droppable({
        drop: function(event, ui) {
            if (currentDrag.parent().hasClass("goods-bg-big")) {
                // currentDrag.css({"top": 0, "left": 0});
            } else {
                currentDrop = $(this);
                hidePopovers();
                currentDrag.css({
                    "top": 0,
                    "left": 0
                });
                currentDrag.removeClass();
                currentDrag.addClass('box-packet popover-right-box draggable');
                currentDrag.draggable("option", "revert", true);
                currentDrop.append(currentDrag);
            }
        },
    });

    $('.car-fache > img').droppable({
        drop: function(event, ui) {
            if (currentDrag.hasClass('box-packet2')) {
                currentDrop = $(this);
                hidePopovers();

                //在此处判断，如果超载，弹出提示框
                $('#alert > .alert-msg').text("超载/超出容积");	//设置弹出框内容
                showModifyNumberPopover($('#alert'), event);	//显示弹出框
                currentDrag.draggable("option", "revert", false);
            }
            
        },
    });
    $('.zone-container').droppable({
        drop: function(event, ui) {
            currentDrop = $(this);
            hidePopovers();

            //在此处判断，如果超处12个货物，弹出提示框
            console.log($(this).children().length);
            if ($(this).children().length >= 12) {
                $('#alert > .alert-msg').text("该分拣区已满，请放入其他分拣区！");
                showModifyNumberPopover($('#alert'), event);
            } else {
                //如果没有超出，添加一个纸箱到分拣区
                currentDrag.css({
                    "top": 0,
                    "left": 0
                });
                currentDrag.removeClass();
                currentDrag.addClass('box-packet2 popover-right-box2 draggable');
                currentDrag.draggable("option", "revert", true);
                currentDrop.append(currentDrag);
                // 或者克隆一个DOM
                // currentDrag.clone().prependTo(currentDrop.find('.box-inside'));
            }
        },
    });
    /*=============================
        绑定拖动事件
    =============================*/
    bindDragableEvent();

    /*=============================
        处理表单提交
	=============================*/
    $('#add-form').submit(function(e) {
        // 在此处更新包装信息
        return false;
    });
    /*=============================
        绑定模态事件
    =============================*/
    $('#modifytrigger').leanModal({
        top: 110,
        overlay: 0.45,
        closeButton: ".hidemodal"
    });


});
/*=============================
        自定义方法
=============================*/
function bindDragableEvent() {
    $(".draggable").draggable({
        revert: true,
        revertDuration: 200,
        cursor: "move",
        scroll: false,
        drag: function() {
            currentDrag = $(this);
            hidePopovers();
        },
    });
    $(".draggable-helper").draggable({
        revert: true,
        revertDuration: 200,
        scroll: false,
        cursor: "move",
        cursorAt: {
            top: 37,
            left: 37
        },
        helper: function(event) {
            return $("<img src='./images/huojiabox.png'>");
        },
        drag: function() {
            // hidePopovers();
            currentDrag = $(this);
        },
    });
}

function showModifyNumberPopover(obj, event) {
    obj.css('display', 'block');
    obj.css('top', event.pageY + 'px');
    obj.css('left', (event.pageX - obj.width() / 2) + 'px');
}
function showGoodsPopover(obj, event, type) {
    hidePopovers();
    obj.css('display', 'block');
    obj.css('top', event.pageY + 'px');
    switch (type) {
    case "left":
        obj.css('left', (event.pageX - obj.width()) + 'px');
        break;
    case "right":
        obj.css('left', event.pageX + 'px');
        break;
    default:
        obj.css('left', (event.pageX - obj.width() / 2) + 'px');
    }
}
function hidePopovers() {
    $('#good-detail').css('display', 'none');
    $('#box-detail').css('display', 'none');
    $('#box-detail-dragable').css('display', 'none');
    $('#box-detail-nomodify').css('display', 'none');
    // $('#alert').css('display', 'none');
}