var currentDrag;
var currentDrop;
$(document).ready(function() {
    //添加鼠标移入和移出事件，通过delegate方法可以实现通过js动态添加的元素自动绑定事件
    $('.baozhuang-container').delegate('.good', 'mouseenter',
        function(ev) {
            var goodPopover = $('#good-detail');
            showGoodsPopover(goodPopover, ev, 'right');

        });
    $('.baozhuang-container').delegate('.good', 'mouseleave',
        function(ev) {
            var goodId = $(this).attr('data-id');
            $('#good-detail').css('display', 'none');
        });

    $('.baozhuang-container').delegate('.popover-left', 'mouseenter',
        function(ev) {
            var goodID = $(this).attr('data-id');
            var goodPopover = $('#good-detail');
            showGoodsPopover(goodPopover, ev, 'left');
        });

    $('.baozhuang-container').delegate('.popover-left', 'mouseleave',
        function() {
            var goodId = $(this).attr('data-id');
            $('#good-detail').css('display', 'none');
        });

    $('.baozhuang-container').delegate('.box-packet', 'mouseenter',
        function(ev) {
            var packetID = $(this).attr('data-id');
            var goodPopover = $('#box-detail');
            showGoodsPopover(goodPopover, ev, 'left');
        });
    $('.baozhuang-container').delegate('.popover-right-box', 'mouseenter',
        function(ev) {
            var packetID = $(this).attr('data-id');
            var goodPopover = $('#box-detail');
            showGoodsPopover(goodPopover, ev, 'right');
        });
    $('.baozhuang-container').delegate('.popover-left-box', 'mouseenter',
        function(ev) {
            var packetID = $(this).attr('data-id');
            var goodPopover = $('#box-detail');
            showGoodsPopover(goodPopover, ev, 'left');
        });

    /*=============================
        关闭弹窗
    =============================*/
    $('#baozhuang-bg').click(function() {
        hidePopovers();
    });

    // 关闭修改数量弹出框
    $('#cancleNumber').click(function() {
        currentDrag.css({
            "top": 0,
            "left": 0
        });
        currentDrag.draggable("option", "revert", true);
        var modifynumber = $('#modifynumber');
        modifynumber.css('display', 'none');
    });
    $('#cancle-alert').click(function() {
        currentDrag.css({
            "top": 0,
            "left": 0
        });
        currentDrag.draggable("option", "revert", true);
        var modifynumber = $('#alert');
        modifynumber.css('display', 'none');
    });

    /*=============================
        处理放下事件
    =============================*/
    $('.box-inside .box-inside-goods').droppable({
        drop: function(event, ui) {
            if (currentDrag.parent().hasClass("box-inside")) {
                // currentDrag.css({"top": 0, "left": 0});
            } else {
                currentDrop = $(this);
                hidePopovers();
                showModifyNumberPopover($('#modifynumber'), event);
                currentDrag.draggable("option", "revert", false);
                var goodId = $(this).attr('data-id');
            }
        },
    });
    $('.goods-bg').droppable({
        drop: function(event, ui) {
            if (currentDrag.parent().hasClass("goods-bg")) {
                // currentDrag.css({"top": 0, "left": 0});
            } else {
                currentDrop = $(this);
                hidePopovers();
                showModifyNumberPopover($('#modifynumber'), event);
                currentDrag.draggable("option", "revert", false);
            }
        },
    });

    /*=============================
        绑定拖动事件
    =============================*/
    bindDragableEvent();

    /*=============================
        处理表单提交
    =============================*/
    $('#modify-form').submit(function(e) {

        return false;
    });
    $('#add-form').submit(function(e) {
        // 货物打包类型，currentDabao是当前打包的区域
        var dabaoType = currentDabao.attr('boxtype');
        var dabaoBox;
        //如果可以打包，根据纸箱类型打包到【包装完成区】
        if (dabaoType == 'little') {
            dabaoBox = '<div class="box-packet popover-left-box" data-id="1" box-type="little"><img src="./images/baozhuang1.png"></div>';
        } else if (dabaoType == 'middle') {
            dabaoBox = '<div class="box-packet popover-left-box" data-id="2" box-type="middle"><img src="./images/baozhuang2.png"></div>';
        } else if (dabaoType == 'larg') {
            dabaoBox = '<div class="box-packet popover-left-box" data-id="3" box-type="larg"><img src="./images/baozhuang3.png"></div>';
        }
        // 包装完成区中加入包装箱
        $('#box-contanier').append(dabaoBox);
        //清空打包区的货物
        currentDabao.find('.box-inside-goods').empty();

        return false;
    });

    $('#numberform').submit(function(e) {
        // 在此处做条件判断，如果满足条件插入货物DOM
        currentDrag.css({
            "top": 0,
            "left": 0
        });
        currentDrag.draggable("option", "revert", true);
        currentDrop.append(currentDrag);
        // 或者克隆一个DOM
        // currentDrag.clone().prependTo(currentDrop.find('.box-inside'));
        $('#modifynumber').css('display', 'none');
        return false;
    });

    /*=============================
        绑定模态事件
    =============================*/
    $('#modifytrigger').leanModal({
        top: 110,
        overlay: 0.45,
        closeButton: ".hidemodal"
    });
    $('.addtrigger').leanModal({
        top: 110,
        overlay: 0.45,
        closeButton: ".hidemodal"
    });

});

function bindDragableEvent() {
    $(".draggable").draggable({
        revert: true,
        revertDuration: 200,
        cursor: "move",
        scroll: false,
        drag: function() {
            currentDrag = $(this);
            hidePopovers();
        },
    });
    $(".draggable-helper").draggable({
        revert: true,
        revertDuration: 200,
        scroll: false,
        cursor: "move",
        cursorAt: {
            top: 37,
            left: 37
        },
        helper: function(event) {
            return $("<img src='./images/huojiabox.png'>");
        },
        drag: function() {
            // hidePopovers();
            currentDrag = $(this);
        },
    });
}

function showModifyNumberPopover(obj, event) {
    obj.css('display', 'block');
    obj.css('top', event.pageY + 'px');
    obj.css('left', (event.pageX - obj.width() / 2) + 'px');
}

function showGoodsPopover(obj, event, type) {
    hidePopovers();
    obj.css('display', 'block');
    obj.css('top', event.pageY + 'px');
    switch (type) {
        case "left":
            obj.css('left', (event.pageX - obj.width()) + 'px');
            break;
        case "right":
            obj.css('left', event.pageX + 'px');
            break;
        default:
            obj.css('left', (event.pageX - obj.width() / 2) + 'px');
    }
}

function hidePopovers() {
    $('#good-detail').css('display', 'none');
    $('#box-detail').css('display', 'none');
    $('#box-detail-dragable').css('display', 'none');
    $('#box-detail-nomodify').css('display', 'none');
    $('#alert').css('display', 'none');
}

$(document).ready(function() {
    $('.car').mouseenter(function(ev) {
        $(this).children('.xiehuoanniu').css('display', 'block');
    });
    $('.car').mouseleave(function() {
        $(this).children('.xiehuoanniu').css('display', 'none');
    });
});