$(document).ready(function() {

    /**************************
		绑定点击事件
	**************************/
    // 绑定【查看今日汇率按钮】
    $('.btn-ckhl').click(function() {
        alert('触发【查看今日汇率】');
        //获取两个币种选择框的value
        var $a = $('#bizhong-a').find('select').val();
        var $b = $('#bizhong-b').find('select').val();

        //更改输入框的汇率值
        $(this).parents('tr').find('input').val(6.2);

    });
    // 为交换按钮绑定事件
	$('.btn-exchange').click(function() {
        var $a = $('#bizhong-a').find('select')
        var $b = $('#bizhong-b').find('select');

        var indexA = $a.get(0).selectedIndex;
        var indexB = $b.get(0).selectedIndex;
        
        $a.get(0).selectedIndex = indexB;
        $b.get(0).selectedIndex = indexA;
        
        $a.trigger('change');
        $b.trigger('change');
    });
    // 为【兑换】按钮绑定事件
    $('.btn-convert').click(function() {
        
        
    });
    $('.jiaoyi-list-bg form').submit(function() {
        // 下面的语句弹出模态提示框
        $('#convert-model-trigger').trigger('click');
        return false;
    });
    /**************************
        设置模态框
    **************************/
    $('#convert-model-trigger').leanModal({
        top: 210,
        overlay: 0.45,
        closeButton: "#hidemodal"
    });
});