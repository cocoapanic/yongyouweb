$(document).ready(function() {
    //显示Alert，样式为2
    showAlertView("请输入正确的数量范围", 2);
    /**************************
		为 + - 绑定点击事件
	**************************/
	$('.jyjl-list-container').delegate('.btn-whhl-add', 'click',
    function(ev) {
        // 在当前行下方插入一行
        $(this).parents('tr').after($('.whhl-temp').clone(true).removeClass());
    });
    $('.jyjl-list-container').delegate('.btn-whhl-reduce', 'click',
    function(ev) {
        // 移除当前行
        var $parent = $(this).parents('tr').remove();
    });

    /**************************
        为保存按钮绑定事件
    **************************/
    $('.jyjl-list-container').delegate('.btn-whhl-save', 'click',
    function(ev) {
        // 执行保存操作
        console.log("保存");
    });
});
