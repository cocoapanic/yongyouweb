$(document).ready(function() {

	$(".sel_wrap").on("change", function() {
		var o;
		var opt = $(this).find('option');
		opt.each(function(i) {
			if (opt[i].selected == true) {
				o = opt[i].innerHTML;
			}
		})
		$(this).find('label').html(o);
	});




	$(".sel_wrap").parents("form").on("reset", function() {
		$(".sel_wrap").each(function() {

			var opt = $(this).find('option');

			$(this).find('label').html(opt[0].innerHTML);
		});
		
		
	})

});