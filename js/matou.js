var currentDrag;
var currentDrop;
$(document).ready(function() {
	//添加鼠标移入和移出事件，通过delegate方法可以实现通过js动态添加的元素自动绑定事件
    $('.matou-zhuangche-bg').delegate('.matou-box', 'mouseenter',
    function(ev) {
        var goodPopover = $('#good-detail');
        showGoodsPopover(goodPopover, ev, 'right');

    });
    $('.matou-zhuangche-bg').delegate('.matou-box', 'mouseleave',
    function(ev) {
        var goodId = $(this).attr('data-id');
        $('#good-detail').css('display', 'none');
    });

    /*=============================
        关闭弹窗
    =============================*/
    $('#cancle-alert').click(function() {
        currentDrag.css({
            "top": 0,
            "left": 0
        });
        currentDrag.draggable("option", "revert", true);
        var modifynumber = $('#alert');
        modifynumber.css('display', 'none');
    });


    /*=============================
        处理放下事件
    =============================*/
    $('.car-fache > img').droppable({
        drop: function(event, ui) {
            currentDrop = $(this);
            //在此处判断，如果超载，弹出提示框
            $('#alert > .alert-msg').text("超载/超出容积");
            showModifyNumberPopover($('#alert'), event);
            currentDrag.draggable("option", "revert", false);
        },
    });

    /*=============================
        绑定拖动事件
    =============================*/
    bindDragableEvent();

});

function bindDragableEvent() {
    $(".draggable").draggable({
        revert: true,
        revertDuration: 200,
        cursor: "move",
        scroll: false,
        drag: function() {
            currentDrag = $(this);
            hidePopovers();
        },
    });
}

function showModifyNumberPopover(obj, event) {
    obj.css('display', 'block');
    obj.css('top', event.pageY + 'px');
    obj.css('left', (event.pageX - obj.width() / 2) + 'px');
}
function showGoodsPopover(obj, event, type) {
    hidePopovers();
    obj.css('display', 'block');
    obj.css('top', event.pageY + 'px');
    switch (type) {
    case "left":
        obj.css('left', (event.pageX - obj.width()) + 'px');
        break;
    case "right":
        obj.css('left', event.pageX + 'px');
        break;
    default:
        obj.css('left', (event.pageX - obj.width() / 2) + 'px');
    }
}
function hidePopovers() {
    $('#good-detail').css('display', 'none');
    $('#alert').css('display', 'none');
}
