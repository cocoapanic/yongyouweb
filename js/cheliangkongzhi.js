var currentSelectedItem;    // 当前被选择的项目
var dom = String('<tr class="cheliang-detail"><td><a href="#" class="jiajian jianjian-first">+</a> <a href="#" class="jiajian">-</a><div class="clear"></div></td><td class="selectlist" id="car"></td><td></td><td class="td-img"><img src="./images/10X9001.png"></td><td class="select-date" data-date-format="yyyy-MM-dd " data-link-field="dtp_input2" data-link-format="yyyy-mm-dd"></td><td class="select-time"></td><td class="selectlist" id="zuzhi"></td><td class="selectlist" id="tingchequ"></td><td class="car-address"></td><td><input type="text" name="lianxiren" id="lianxiren" class="txtfield" placeholder="联系人"></td><td><a href="" class="xiada-button">下达</a></td></tr>');
$(document).ready(function() {
    // 给【选择车辆】、【组织】和【停车】选择框加点击事件
    $('.table-container').delegate('.selectlist', 'click', function(ev) {
        currentSelectedItem = ev.currentTarget;
        var objID = ev.currentTarget.id;
        if (objID == "car") {
            //获取货物车辆弹出框
            visiblePopover = $('#select-car');
        } else if(objID == "zuzhi"){
            $('#zuzhi-model-trigger').trigger('click');
            return
        }else {
            visiblePopover = $('#select-tingche');
        }
        showPopover(visiblePopover, ev, 'right');
    });
    // 为 + - 号绑定事件
    $('.table-container').delegate('.jiajian', 'click', function(ev) {
        //如果用户点击+
        if ($(this).html() == "+") {
            // 在此处进行添加行为
            addRow($(this).parents('.cheliang-detail'));
            
        }else {//如果用户点击-

            $(this).parents('.cheliang-detail').remove();
            if ($('.wuliu-container table tbody').children().length == 0) {
                $('.wuliu-container table tfoot').css('display', 'table-footer-group');
            }
        }
    });
    // 用户点击选择框内选项的事件 
    $('.select-popover').delegate('p', 'click', function(ev) {
        // 获取选项内容
        var selectItem = ev.currentTarget.innerHTML;
        // 更新选择项
        currentSelectedItem.innerHTML = selectItem;

        if (currentSelectedItem.id == 'car') {
            $(currentSelectedItem).next().html(ev.currentTarget.id);
            // 更新车辆图片，注：你们需要在这里设置车辆类型和车辆图片的对应关系，这里我只是写了一个例子，车辆编号对于图片文件名。
            $(currentSelectedItem).nextAll('.td-img').html("<img src='./images/" + ev.currentTarget.id + ".png'>");
            // 在这里可以修改车辆的地址
            $(currentSelectedItem).nextAll('.car-address').html("北京xxxxxx");
            console.log($(currentSelectedItem).nextAll('.car-address'));
        }

        hidePopovers();
    });

    // 为下达按钮绑定事件
    $('.table-container').delegate('xiada-button', 'click', function(ev) {
        
    });

    $('.wuliu-container table tfoot a').click(function(ev) {
        $('.wuliu-container table tbody').append(dom);
        bindDatePicker();
        //隐藏【+增加一行】
        $(this).parents('tfoot').css('display', 'none');
    });

    bindDatePicker();
    /**************************
        设置模态框
    **************************/
    $('#zuzhi-model-trigger').leanModal({
        top: 210,
        overlay: 0.45,
        closeButton: ".select-popover p"
    });

});

/*
    addRow : 在obj增加一行
*/
function addRow (obj) {
    obj.after(dom);
    bindDatePicker();
}
/*
    绑定日期选择器
*/
function bindDatePicker () {

    $('.select-date').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    }).on('changeDate', function(ev){
        var year = ev.date.getUTCFullYear();
        var month = buquan(ev.date.getUTCMonth() + 1, 2);
        var dd = buquan(ev.date.getUTCDate(), 2);

        $(this).html(year+'-'+month+'-'+dd);
    });
    $('.select-time').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    }).on('changeDate', function(ev){
        var hours = buquan(ev.date.getUTCHours(), 2);
        var minutes = buquan(ev.date.getUTCMinutes(), 2);

        $(this).html(hours + ':' + minutes);
    });
}
/*
    showPopover : 显示弹出框
    @obj    将要显示的弹出框对象
    @event  触发的事件
    @type   弹出的位置，左、右（left、right），默认为中 
*/
function showPopover(obj, event, type) {
    // ********创建遮罩********
    var overlay = $("<div id='lean_overlay'></div>");
    $("body").append(overlay);
    $("#lean_overlay").click(function() {
        $("#lean_overlay").fadeOut(200);
        hidePopovers();
    });
    $("#lean_overlay").css({
        "display": "block",
        "opacity": 0
    });
    obj.css({
        'display': 'block',
        'top': event.pageY + 'px',
        "z-index": 11111
    });
    // ********固定弹出框位置********
    switch (type) {
    case "left":
        obj.css('left', (event.pageX - obj.width()) + 'px');
        break;
    case "right":
        obj.css('left', event.pageX + 'px');
        break;
    default:
        obj.css('left', (event.pageX - obj.width() / 2) + 'px');
    }
}
/*
    hidePopovers : 隐藏Class=.select-popover的弹出框
*/
function hidePopovers() {
    $("#lean_overlay").remove();
    $('.select-popover').css('display', 'none');
}


/*
    buquan : 补全前导零
*/
function buquan(num,length){
    var numstr = num.toString();
    var l=numstr.length;
    if (numstr.length>=length) {return numstr;}
      
    for(var  i = 0 ;i<length - l;i++){
      numstr = "0" + numstr;  
     }
    return numstr; 
 }


