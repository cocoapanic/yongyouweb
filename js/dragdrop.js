var currentDrag;
var currentDrop;
$(document).ready(function() {
    $('.baozhuang-container').delegate('.good', 'mouseenter',
    function(ev) {
        var goodPopover = $('#good-detail');
        showGoodsPopover(goodPopover, ev, 'right');

    });
    $('.baozhuang-container').delegate('.good', 'mouseleave',
    function(ev) {
        var goodId = $(this).attr('data-id');
        $('#good-detail').css('display', 'none');
    });

    $('.baozhuang-container').delegate('.popover-left', 'mouseenter',
    function(ev) {
        var goodID = $(this).attr('data-id');
        var goodPopover = $('#good-detail');
        showGoodsPopover(goodPopover, ev, 'left');
    });

    $('.baozhuang-container').delegate('.popover-left', 'mouseleave',
    function() {
        var goodId = $(this).attr('data-id');
        $('#good-detail').css('display', 'none');
    });

    $('.baozhuang-container').delegate('.box-packet', 'mouseenter',
    function(ev) {
        var packetID = $(this).attr('data-id');
        var goodPopover = $('#box-detail');
        showGoodsPopover(goodPopover, ev, 'left');
    });
    $('.baozhuang-container').delegate('.popover-right-box', 'mouseenter',
    function(ev) {
        var packetID = $(this).attr('data-id');
        var goodPopover = $('#box-detail');
        showGoodsPopover(goodPopover, ev, 'right');
    });
    $('.baozhuang-container').delegate('.popover-left-box', 'mouseenter',
    function(ev) {
        var packetID = $(this).attr('data-id');
        var goodPopover = $('#box-detail');
        showGoodsPopover(goodPopover, ev, 'left');
    });
    $('.baozhuang-container').delegate('.popover-right-box2', 'mouseenter',
    function(ev) {
        var packetID = $(this).attr('data-id');
        var goodPopover = $('#box-detail-nomodify');
        showGoodsPopover(goodPopover, ev, 'right');
    });
    $('.baozhuang-container').delegate('.popover-left-box2', 'mouseenter',
    function(ev) {
        var packetID = $(this).attr('data-id');
        var goodPopover = $('#box-detail-nomodify');
        showGoodsPopover(goodPopover, ev, '');
    });
    $('.baozhuang-container').delegate('.good-noempty', 'mouseenter',
    function(ev) {
        showGoodsPopover($('#box-detail-dragable'), ev, "right");
    });

    //关闭弹窗
    $('#baozhuang-bg').click(function() {
        hidePopovers();
    });

    // 打包
    $('.dabao').click(function() {
        var boxInside = $(this).next();
        //在此处判断是否可以打包
        //根据纸箱类型打包到【包装完成区】
        if (boxInside.is('.box-type-little')) {
            $(".box-packet[box-type='little']:first").clone().appendTo($('#box-contanier'));
        } else if (boxInside.is('.box-type-middle')) {
            $(".box-packet[box-type='middle']:first").clone().appendTo($('#box-contanier'));
        } else if (boxInside.is('.box-type-larg')) {
            $(".box-packet[box-type='larg']:first").clone().appendTo($('#box-contanier'));
        }
        //清空打包区的货物
        boxInside.empty();

    });
    // 关闭修改数量弹出框
    $('#cancleNumber').click(function() {
        currentDrag.css({
            "top": 0,
            "left": 0
        });
        currentDrag.draggable("option", "revert", true);
        var modifynumber = $('#modifynumber');
        modifynumber.css('display', 'none');
    });
    $('#cancle-alert').click(function() {
        currentDrag.css({
            "top": 0,
            "left": 0
        });
        currentDrag.draggable("option", "revert", true);
        var modifynumber = $('#alert');
        modifynumber.css('display', 'none');
    });

    // 处理拖拽事件
    $('.box-inside').droppable({
        drop: function(event, ui) {
            if (currentDrag.parent().hasClass("box-inside")) {
                // currentDrag.css({"top": 0, "left": 0});
            } else {
                currentDrop = $(this);
                hidePopovers();
                showModifyNumberPopover($('#modifynumber'), event);
                currentDrag.draggable("option", "revert", false);
                var goodId = $(this).attr('data-id');
            }
        },
    });
    $('.goods-bg').droppable({
        drop: function(event, ui) {
            if (currentDrag.parent().hasClass("goods-bg")) {
                // currentDrag.css({"top": 0, "left": 0});
            } else {
                currentDrop = $(this);
                hidePopovers();
                showModifyNumberPopover($('#modifynumber'), event);
                currentDrag.draggable("option", "revert", false);
            }
        },
    });

    $('.goods-bg-big').droppable({
        drop: function(event, ui) {
            if (currentDrag.parent().hasClass("goods-bg-big")) {
                // currentDrag.css({"top": 0, "left": 0});
            } else {
                currentDrop = $(this);
                hidePopovers();
                currentDrag.css({
                    "top": 0,
                    "left": 0
                });
                currentDrag.removeClass();
                currentDrag.addClass('box-packet popover-right-box draggable');
                currentDrag.draggable("option", "revert", true);
                currentDrop.append(currentDrag);
            }
        },
    });
    $('.kuwei-container').droppable({
        drop: function(event, ui) {
            currentDrop = $(this);
            hidePopovers();
            showModifyNumberPopover($('#modifynumber'), event);
            currentDrag.draggable("option", "revert", false);
        },
    });
    $('.car-fache > img').droppable({
        drop: function(event, ui) {
            currentDrop = $(this);
            hidePopovers();

            //在此处判断，如果超载，弹出提示框
            $('#alert > .alert-msg').text("超载/超出容积");
            showModifyNumberPopover($('#alert'), event);
            currentDrag.draggable("option", "revert", false);
        },
    });
    $('.zone-container').droppable({
        drop: function(event, ui) {
            currentDrop = $(this);
            hidePopovers();

            //在此处判断，如果超处12个货物，弹出提示框
            console.log($(this).children().length);
            if ($(this).children().length >= 12) {
                $('#alert > .alert-msg').text("该分拣区已满，请放入其他分拣区！");
                showModifyNumberPopover($('#alert'), event);
            } else {
                //如果没有超出，添加一个纸箱到分拣区
                currentDrag.css({
                    "top": 0,
                    "left": 0
                });
                currentDrag.removeClass();
                currentDrag.addClass('box-packet2 popover-right-box2 draggable');
                currentDrag.draggable("option", "revert", true);
                currentDrop.append(currentDrag);
                // 或者克隆一个DOM
                // currentDrag.clone().prependTo(currentDrop.find('.box-inside'));
            }
        },
    });
    //绑定拖拽事件
    bindDragableEvent();
    // 处理表单提交
    $('#addressform').submit(function(e) {
        return false;
    });
    $('#numberform').submit(function(e) {
        // 在此处做条件判断，如果满足条件插入货物DOM
        currentDrag.css({
            "top": 0,
            "left": 0
        });
        currentDrag.draggable("option", "revert", true);
        currentDrop.append(currentDrag);
        // 或者克隆一个DOM
        // currentDrag.clone().prependTo(currentDrop.find('.box-inside'));
        $('#modifynumber').css('display', 'none');
        return false;
    });
    $('#numberform-zh').submit(function(e) {
        // 判断是待包装区，还是库位
        if (currentDrop.is('box-contanier')) {
          currentDrop.append('<div class="good draggable popover-left ui-draggable ui-draggable-handle" data-id="1" style="position: relative;"><img src="./images/good1.png"></div>')
        } else if (currentDrop.is('kuwei-container')){
          // 在此处做条件判断，如果满足条件，将货物的信息插入到 模型 里
          currentDrag.remove();
          currentDrop.removeClass('good-empty');
          currentDrop.addClass('good-noempty');

        }
        $('#modifynumber').css('display', 'none');
        return false;
    });
    // 处理Modal事件
    $('#modaltrigger').leanModal({
        top: 110,
        overlay: 0.45,
        closeButton: ".hidemodal"
    });

});

function bindDragableEvent() {
    $(".draggable").draggable({
        revert: true,
        revertDuration: 200,
        cursor: "move",
        scroll: false,
        drag: function() {
            currentDrag = $(this);
            hidePopovers();
        },
    });
    $(".draggable-helper").draggable({
        revert: true,
        revertDuration: 200,
        scroll: false,
        cursor: "move",
        cursorAt: {
            top: 37,
            left: 37
        },
        helper: function(event) {
            return $("<img src='./images/huojiabox.png'>");
        },
        drag: function() {
            // hidePopovers();
            currentDrag = $(this);
        },
    });
}

function showModifyNumberPopover(obj, event) {
    obj.css('display', 'block');
    obj.css('top', event.pageY + 'px');
    obj.css('left', (event.pageX - obj.width() / 2) + 'px');
}
function showGoodsPopover(obj, event, type) {
    hidePopovers();
    obj.css('display', 'block');
    obj.css('top', event.pageY + 'px');
    switch (type) {
    case "left":
        obj.css('left', (event.pageX - obj.width()) + 'px');
        break;
    case "right":
        obj.css('left', event.pageX + 'px');
        break;
    default:
        obj.css('left', (event.pageX - obj.width() / 2) + 'px');
    }
}
function hidePopovers() {
    $('#good-detail').css('display', 'none');
    $('#box-detail').css('display', 'none');
    $('#box-detail-dragable').css('display', 'none');
    $('#box-detail-nomodify').css('display', 'none');
    $('#alert').css('display', 'none');
}

$(document).ready(function() {
    $('.car').mouseenter(function(ev) {
        $(this).children('.xiehuoanniu').css('display', 'block');
    });
    $('.car').mouseleave(function() {
        $(this).children('.xiehuoanniu').css('display', 'none');
    });
});