$("ul.help-index .help-item").each(function (index) {
	$(this).click(function() {
        var liNode = $(this);  
		$("div.help-content-show").removeClass("help-content-show");  
        //将原来有tabin属性的标签去掉tabin属性  
        $("ul.help-index li.help-item-hover").removeClass("help-item-hover");  
        //将当前标签对应的内容区域显示出来  
        $("div.help-content").eq(index).addClass("help-content-show");  
        liNode.addClass("help-item-hover"); 
	})
})


$("ul.work-index .work-item-box").each(function (index) {
	$(this).click(function() {
        var liNode = $(this);  
		$("div.work-content-show").removeClass("work-content-show");    
        $("ul.work-index li.work-item-box-hover").removeClass("work-item-box-hover");  
        $("ul.work-index div.work-item-hover").removeClass("work-item-hover");  
        $("div.work-content").eq(index).addClass("work-content-show");  
        liNode.addClass("work-item-box-hover"); 
        liNode.children().first().addClass("work-item-hover");
	})
})