/*
    1、在HTML文件中将该文件引入到jQuery下面和需要调用该方法的文件的上面
    例如：
    <script src="./js/jquery-1.11.2.min.js"></script>
    <script src="./js/alert.js"></script>
    <script src="./js/matou.js"></script>
    
    2、需要显示Alert时，调用showAlertView(msg)，参数1是要显示的内容，参数2是alert的样式。

*/
function showAlertView (message, type) {
	var overlay = $("<div id='lean_overlay'></div>");
	var alertDom = '<div id="alertModel" class="popover2"><h1>提醒</h1><p class="alert-msg">' + message + '</p><div id="closeAlertModel" class="hidemodal2 cancle2">确定</div></div>'
    var alertDom2 = '<div id="alertModel" class="alert-model2"><div class="modal-title">提醒<a href="javascript:void(0)" id="closeAlertModel" class="hidemodal3"></a></div><h2>' + message + '</h2></div>'
    $("body").append(overlay);
    switch (type) {
        case 1:
            $('body').append(alertDom);
            break;
        case 2:
            $('body').append(alertDom2);
            break;
        default:
            $('body').append(alertDom);
    }

    $("#lean_overlay").click(function() {
        close_modal('#alertModel');
    });
    $('#closeAlertModel').click(function() {
        close_modal('#alertModel');
    });
	$('#alertModel').css({
		'display': 'block',
		'top': '200px'
	});

	$("#lean_overlay").css({
        "display": "block",
        opacity: 0
    });
    var modal_height = $('#alertModel').outerHeight();
    var modal_width = $('#alertModel').outerWidth();
    $("#lean_overlay").fadeTo(200, 0.5);
    $('#alertModel').css({
        "display": "block",
        "position": "fixed",
        "opacity": 0,
        "z-index": 11000,
        "left": 50 + "%",
        "margin-left": -(modal_width / 2) + "px",
        "top": 200 + "px"
    });
    $('#alertModel').fadeTo(200, 1);

}
function close_modal(modal_id) {
    $("#lean_overlay").fadeOut(200);
    $('#alertModel').css({
        "display": "none"
    })
    $("#lean_overlay").remove();
    $('#alertModel').remove();
}