$(document).ready(function() {
    /**************************
		为 + - 绑定点击事件
	**************************/
	$('.whhl-table').delegate('.btn-whhl-add', 'click',
    function(ev) {
        // 在当前行下方插入一行
        $(this).parents('tr').after($('.whhl-temp').clone(true).removeClass());
    });
    $('.whhl-table').delegate('.btn-whhl-reduce', 'click',
    function(ev) {
        // 移除当前行
        var $parent = $(this).parents('tr').remove();
    });

    /**************************
        为保存按钮绑定事件
    **************************/
    $('.whhl-table').delegate('.btn-whhl-save', 'click',
    function(ev) {
        // 执行保存操作
        console.log("保存");
    });
});
