/*
    bindDragableEvent():为可以拖动的元素绑定拖动事件，这个方法需要在动态添加可拖动元素后手动调用。 
*/

var currentDrag;
var currentDrop;
var isShowGoodDetail;
$(document).ready(function() {
    // 为展开按钮添加事件        
    $(".zhankai").click(function(event) {
        if ($(this).html() === "展开") {
            $(".xiehuo-container .box-bg").toggleClass('togglable-height');
            $(this).html("收起");
            $(".side .goods-bg").height($(".xiehuo-container .box-bg").height() + 25);
        } else{
            $(".xiehuo-container .box-bg").toggleClass('togglable-height');
            $(".side .goods-bg").height("487px");

            $(this).html("展开");

        }
    });
    //添加汽车上的卸货按钮
    $('.xiehuo-container').delegate('.car', 'mouseenter', function(ev) {
        $(this).children('.xiehuoanniu').css('display', 'block');
    });
    $('.xiehuo-container').delegate('.car', 'mouseleave', function() {
        $(this).children('.xiehuoanniu').css('display', 'none');
    });

    // 添加鼠标移入事件
    $('.xiehuo-container').delegate('.good', 'mouseenter',
    function(ev) {
        var goodPopover = $('#good-detail');
        showGoodsPopover(goodPopover, ev, 'right');

    });
    $('.xiehuo-container').delegate('.good', 'mouseleave',
    function(ev) {
        var goodId = $(this).attr('data-id');
        $('#good-detail').css('display', 'none');
    });

    $('.xiehuo-container').delegate('.popover-left', 'mouseenter',
    function(ev) {
        var goodID = $(this).attr('data-id');
        var goodPopover = $('#good-detail');
        showGoodsPopover(goodPopover, ev, 'left');
    });

    $('.xiehuo-container').delegate('.popover-left', 'mouseleave',
    function() {
        var goodId = $(this).attr('data-id');
        $('#good-detail').css('display', 'none');
    });

    $('.xiehuo-container').delegate('.good-noempty', 'mouseenter',
    function(ev) {

        showGoodsPopover($('#box-detail-dragable'), ev, "right");
    });

    /*=============================
        关闭弹窗
    =============================*/
    $('#baozhuang-bg').click(function(event) {
        hidePopovers();
    });

    // 关闭修改数量弹出框
    $('#cancleNumber').click(function() {
        currentDrag.css({
            "top": 0,
            "left": 0
        });
        currentDrag.draggable("option", "revert", true);
        var modifynumber = $('#modifynumber');
        modifynumber.css('display', 'none');
    });

    /*=============================
        处理放下事件
    =============================*/
    $('#good-drop').droppable({
        drop: function(event, ui) {
            if ($(this).hasClass("nonedrop")) {
                return;
            }
            if (currentDrag.parent().hasClass("goods-bg")) {

                // currentDrag.css({"top": 0, "left": 0});
            } else {
                currentDrop = $(this);
                hidePopovers();
                showModifyNumberPopover($('#modifynumber'), event);
                currentDrag.draggable("option", "revert", false);
            }
        },
    });
    $('#box-drop').droppable({
        drop: function(event, ui) {
            if (currentDrag.parent().hasClass("goods-bg")) {
                // currentDrag.css({"top": 0, "left": 0});
            } else {
                currentDrop = $(this);
                hidePopovers();
                showModifyNumberPopover($('#modifynumber'), event);
                currentDrag.draggable("option", "revert", false);
            }
        },
    });
    $('.kuwei-container').droppable({
        drop: function(event, ui) {
            currentDrop = $(this);
            hidePopovers();
            showModifyNumberPopover($('#modifynumber'), event);
            currentDrag.draggable("option", "revert", false);
        },
    });
    /*=============================
        绑定拖动事件
    =============================*/
    bindDragableEvent();

    /*=============================
        处理表单提交
    =============================*/
    $('#numberform-zh').submit(function(e) {
        var isMoveGood = true;  //是否移动货物
        // 1、判断用户输入的数量是否满足条件，修改货物来源区域的货物DOM，100替换为货物的实际数量。
        var userInputNumber = $('#goodnumber').val()
        if ( userInputNumber > 100) {
            // 提示用户输入的数量有误
            isMoveGood = false;
            currentDrag.css({
                "top": 0,
                "left": 0
            });
        }else if(userInputNumber == 100){
            // 从货物来源区域删除货物的DOM
            currentDrag.remove();
        }else {
            // 在此处修改原始货物的属性（数量）
            currentDrag.css({
                "top": 0,
                "left": 0
            });
        }

        if (isMoveGood) {
            // 2、向目标区域添加货物的DOM，判断放置的区域是待包装区，还是库位
            if (currentDrop.is('#box-drop')) {
                //向包装区中插入货物
                currentDrop.append('<div class="good draggable popover-left ui-draggable ui-draggable-handle" data-id="1" style="position: relative;"><img src="./images/good1.png"></div>')

            } else if (currentDrop.is('.kuwei-container')) {
                // 在此处做条件判断，如果满足条件，将货物的信息插入到 模型 里
                currentDrop.removeClass('good-empty');
                currentDrop.addClass('good-noempty');
            }
        }

        // 隐藏修改数量弹出框
        $('#modifynumber').css('display', 'none');
        return false;
    });

});

//该方法需要每次添加可拖拽元素之后调用
function bindDragableEvent() {
    $(".draggable").draggable({
        revert: true,
        revertDuration: 200,
        cursor: "move",
        scroll: false,
        drag: function() {
            currentDrag = $(this);
            hidePopovers();
        },
    });
    $(".draggable-helper").draggable({
        revert: true,
        revertDuration: 200,
        scroll: false,
        cursor: "move",
        cursorAt: {
            top: 250,
            left: 250
        },
        helper: function(event) {
            return $("<div style='width:500px;height:500px;line-height:550px;text-align:center;'><img src='./images/huojiabox.png'></div>");
        },
        drag: function() {
            // hidePopovers();
            currentDrag = $(this);
        },
    });
}

function showModifyNumberPopover(obj, event) {
    obj.find('#goodnumber').val(null);
    obj.css('display', 'block');
    obj.css('top', event.pageY + 'px');
    obj.css('left', (event.pageX - obj.width() / 2) + 'px');
}
function showGoodsPopover(obj, event, type) {
    hidePopovers();
    obj.css('display', 'block');
    obj.css('top', event.pageY + 'px');
    switch (type) {
    case "left":
        obj.css('left', (event.pageX - obj.width()) + 'px');
        break;
    case "right":
        obj.css('left', event.pageX + 'px');
        break;
    default:
        obj.css('left', (event.pageX - obj.width() / 2) + 'px');
    }
}
function hidePopovers() {
    $('#good-detail').css('display', 'none');
    $('#box-detail').css('display', 'none');
    $('#box-detail-dragable').css('display', 'none');
    $('#box-detail-nomodify').css('display', 'none');
    $('#alert').css('display', 'none');
}