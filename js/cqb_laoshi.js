$(document).ready(function() {
    showAlertView('123123123123123123123123123123123123123123', 1);
	/**************************
		绑定事件
	**************************/
	$('.tables').delegate('.add-btn', 'click', 
    function(ev) {
        $(this).parents('tr').after($(this).parents('tr').clone());
        randerTableColor();
        // 此处进行添加航线操作
        bindDatePicker();

    });
    $('.tables').delegate('.del-btn', 'click', 
    function(ev) {
        // 此处进行删除航线操作

        // 删除之后调用下面的语句删除当前所在行
        $(this).parents('tr').remove();

    });

    /**************************
		为收款人名册绑定点击事件
	**************************/
	$('.skr-list-content ul').delegate('li', 'click',
    function(ev) {
        var option = $(this).find('p span');
       	$('#skrzh').val(option[1].innerHTML);
       	$('#skrhm').val(option[0].innerHTML);
        hideModel();
    });

    bindDatePicker();
    $('#datetimepicker').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
});


/*
    绑定日期选择器
*/
function bindDatePicker () {
    $('.pick-date').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    }).on('changeDate', function(ev){
        var year = ev.date.getUTCFullYear().toString();
        var month = buquan(ev.date.getUTCMonth() + 1, 2);
        var dd = buquan(ev.date.getUTCDate(), 2);

        $(this).html(month+'/'+dd+'/'+year[2]+year[3]);
    });
}

/*
    buquan : 补全前导零
*/
function buquan(num,length){
    var numstr = num.toString();
    var l=numstr.length;
    if (numstr.length>=length) {return numstr;}
      
    for(var  i = 0 ;i<length - l;i++){
      numstr = "0" + numstr;  
     }
    return numstr; 
 }